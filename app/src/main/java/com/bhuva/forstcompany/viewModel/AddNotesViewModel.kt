package com.bhuva.forstcompany.viewModel

import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.bhuva.forstcompany.database.NotesEntity
import com.bhuva.forstcompany.repository.NotesRepository
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AddNotesViewModel(val notesRepository: NotesRepository) : ViewModel() {

    var title: ObservableField<String>? = ObservableField("")
    var description: ObservableField<String>? = ObservableField("")
    var noteId: ObservableField<Int>? = ObservableField()

    val date = getCurrentDateTime()
    val dateInString = date.toString("yyyy/MM/dd")
    val time = date.toString("HH:mm")
    val mIsOnclick = MutableLiveData<Boolean>()
    var note = 0

    private var _saveDetails = MutableLiveData<NotesEntity>()
    val saveDetails: LiveData<NotesEntity>
        get() = _saveDetails


    @Suppress("UNCHECKED_CAST")
    class Factory @Inject constructor(val notesRepository: NotesRepository) :
            ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AddNotesViewModel::class.java))
                return AddNotesViewModel(notesRepository) as T
            throw IllegalArgumentException("Unknown viewmodel class")
        }
    }


    fun onClick() {
        viewModelScope.launch {
            note++
            noteId?.set(note)
            notesRepository.insertNotes(title?.get()!!, noteId?.get()!!, description?.get()!!, dateInString, time)
            mIsOnclick.value = true


        }


    }

    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

    fun resetValue() {
        mIsOnclick.value = false
    }
}