package com.bhuva.forstcompany.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class NotesEntity (
    var title: String = "",
    @PrimaryKey
    var noteId: Int ,
    var description: String = "",
    var date: String = "",
    var time: String = "")