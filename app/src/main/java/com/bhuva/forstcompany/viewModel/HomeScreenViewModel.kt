package com.bhuva.forstcompany.viewModel

import androidx.lifecycle.*
import com.bhuva.forstcompany.database.NotesEntity
import com.bhuva.forstcompany.repository.NotesRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeScreenViewModel(val notesRepository: NotesRepository) : ViewModel() {
    val mIsActivityChange = MutableLiveData<Boolean>()
    val isActivityChange: LiveData<Boolean> get() = mIsActivityChange


    @Suppress("UNCHECKED_CAST")
    class Factory @Inject constructor(val notesRepository: NotesRepository) :
            ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeScreenViewModel::class.java))
                return HomeScreenViewModel(notesRepository) as T
            throw IllegalArgumentException("Unknown viewmodel class")
        }
    }

    fun OnClick() {
        mIsActivityChange.value = true
    }

    fun getNotesValue(): LiveData<List<NotesEntity>> {
        var data = notesRepository.getNotesValue()
        return data
    }

    fun resetValue() {
        mIsActivityChange.value = false

    }
}