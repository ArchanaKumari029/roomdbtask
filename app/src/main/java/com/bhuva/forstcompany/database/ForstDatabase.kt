package com.bhuva.forstcompany.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [NotesEntity::class],version = 1,exportSchema = false)
abstract class ForstDatabase : RoomDatabase() {

    abstract fun notesDao(): NotesDao
}