package com.bhuva.forstcompany.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bhuva.forstcompany.R
import com.bhuva.forstcompany.database.NotesEntity
import com.bhuva.forstcompany.databinding.ActivityAddNotesBinding
import com.bhuva.forstcompany.databinding.AdapterHomeBinding

class NotesListAdapter(var list:List<NotesEntity>):RecyclerView.Adapter<NotesListAdapter.MyViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {



        return MyViewHolder(AdapterHomeBinding.inflate(LayoutInflater.from(parent.context)))

    }


    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvTitle.text=list[position].title
        holder.binding.tvDescription.text=list[position].description
   }

    class MyViewHolder(val binding: AdapterHomeBinding) :
            RecyclerView.ViewHolder(binding.root){
    }
}