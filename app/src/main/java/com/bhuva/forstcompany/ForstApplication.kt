package com.bhuva.forstcompany

import android.app.Application
import android.content.Context
import com.aetna.di.AppComponent
import com.aetna.di.AppModule
import com.aetna.di.DaggerAppComponent
import com.aetna.di.ViewModelModule

class ForstApplication : Application() {
    private lateinit var appComponent: AppComponent
    private var appContext: Context? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .viewModelModule(ViewModelModule(this))
                .build()
        appContext = applicationContext
        appComponent.Inject(this)

    }

    fun getComponent(): AppComponent {
        return appComponent
    }


}