package com.bhuva.forstcompany.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface NotesDao {
    @Insert(onConflict = REPLACE)
    fun insert(notesEntity: NotesEntity?)

    @Delete
    fun delete(notesEntity: NotesEntity?)

    @Query("DELETE FROM NotesEntity WHERE noteId =:noteId")
    fun deleteUser(noteId: String)

    @Query("SELECT * FROM NotesEntity ")
    fun getUserData(): LiveData<List<NotesEntity>>

}