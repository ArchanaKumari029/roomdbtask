package com.bhuva.forstcompany.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bhuva.forstcompany.ForstApplication
import com.bhuva.forstcompany.R
import com.bhuva.forstcompany.databinding.ActivityAddNotesBinding
import com.bhuva.forstcompany.viewModel.AddNotesViewModel
import javax.inject.Inject


class AddNotesActivity : AppCompatActivity() {
    private lateinit var addNotesModel: AddNotesViewModel


    @Inject
    lateinit var factory: AddNotesViewModel.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityAddNotesBinding: ActivityAddNotesBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_notes)

        (application as ForstApplication).getComponent().Inject(this)

        addNotesModel = ViewModelProviders.of(this, factory).get(AddNotesViewModel::class.java)



        activityAddNotesBinding.model = addNotesModel


        addNotesModel.mIsOnclick.observe(this, Observer {
            if (it){
                addNotesModel.resetValue()
        }
        })


    }
}