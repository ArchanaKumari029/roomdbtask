package com.bhuva.forstcompany.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bhuva.forstcompany.ForstApplication
import com.bhuva.forstcompany.R
import com.bhuva.forstcompany.databinding.ActivityHomeBinding
import com.bhuva.forstcompany.viewModel.AddNotesViewModel
import com.bhuva.forstcompany.viewModel.HomeScreenViewModel
import javax.inject.Inject

class HomeScreenActivity : AppCompatActivity() {
    private lateinit var homeScreenViewModel: HomeScreenViewModel

    @Inject
    lateinit var homeScreenViewModelFactory: HomeScreenViewModel.Factory
    private lateinit var addNotesModel: AddNotesViewModel


    @Inject
    lateinit var factory: AddNotesViewModel.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityHomeBinding: ActivityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        (application as ForstApplication).getComponent().Inject(this)

        homeScreenViewModel = ViewModelProviders.of(this, homeScreenViewModelFactory).get(HomeScreenViewModel::class.java)
        addNotesModel = ViewModelProviders.of(this, factory).get(AddNotesViewModel::class.java)



        activityHomeBinding.model = homeScreenViewModel
        activityHomeBinding.lifecycleOwner = this



        homeScreenViewModel.isActivityChange.observe(this, Observer {

            if (it) {
                var intent = Intent(this, AddNotesActivity::class.java)
                startActivity(intent)
                homeScreenViewModel.resetValue()


            }
        })



        homeScreenViewModel.getNotesValue().observe(this, Observer {


            print("data from db${it}")
            activityHomeBinding.rvNotes.adapter=NotesListAdapter(it)
        })
    }
}