package com.bhuva.forstcompany.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.room.PrimaryKey
import com.bhuva.forstcompany.AppExecutors
import com.bhuva.forstcompany.database.NotesDao
import com.bhuva.forstcompany.database.NotesEntity
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class NotesRepository @Inject constructor(private var appExecutors: AppExecutors,
                                          var notesDao: NotesDao) {


    fun insertNotes(title: String, noteId: Int, description: String, date: String, time: String) {

        appExecutors.diskIO().execute {


            var model = NotesEntity(title, noteId, description, date, time)
            notesDao.insert(model)

        }


    }

    fun getNotesValue(): LiveData<List<NotesEntity>> {


        val mediatorLiveData = MediatorLiveData<List<NotesEntity>>()

        appExecutors.diskIO().execute {


            mediatorLiveData.addSource(notesDao.getUserData())
            {
                it?.let {
                    mediatorLiveData.postValue(it)
                }
            }
        }
        return mediatorLiveData
    }

}