package com.aetna.di

import com.bhuva.forstcompany.ForstApplication
import com.bhuva.forstcompany.repository.NotesRepository
import com.bhuva.forstcompany.ui.AddNotesActivity
import com.bhuva.forstcompany.ui.HomeScreenActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ViewModelModule::class])
interface AppComponent {

    fun Inject(activity: HomeScreenActivity)

    fun Inject(activity: AddNotesActivity)

    fun Inject(repository: NotesRepository)

    fun Inject(application: ForstApplication)


}