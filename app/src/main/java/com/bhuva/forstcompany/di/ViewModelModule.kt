package com.aetna.di

import android.app.Application
import androidx.room.Room
import com.bhuva.forstcompany.database.ForstDatabase
import com.bhuva.forstcompany.database.NotesDao
import com.bhuva.forstcompany.util.AppConfiguration
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule(application: Application) {

    private val forstDatabase: ForstDatabase =
            Room.databaseBuilder(application, ForstDatabase::class.java, AppConfiguration.DB_NAME)
                    .build()

    @Provides
    @Singleton
    fun provideDatabase(): ForstDatabase {
        return forstDatabase
    }

    @Provides
    @Singleton
    fun provideDao(): NotesDao {
        return forstDatabase.notesDao()
    }



}